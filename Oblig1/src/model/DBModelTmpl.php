<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
            try {
                $this->db = new PDO('mysql:host=localhost;dbname=test','root', '');
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (Exception $e) {
                echo "Error accured creating new PDO..";
                echo $e->getMessage();
            }

		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        try {
            $query = $this->db->prepare('SELECT * FROM book');
            $query->execute();
            $rows = $query->fetchAll(PDO::FETCH_ASSOC);

            foreach ($rows as $key=>$row) {
                $booklist[$key] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
            }
            return $booklist;
        } catch (Exception $e) {
            $e->getMessage();
        }

    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		try {
		    $query = $this->db->prepare("SELECT * FROM book WHERE id =:id");
            $query->bindValue(':id', $id);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_OBJ);
            $book = new Book($row->title, $row->author, $row->description, $row->id);
            return $book;
		} catch (Exception $e) {
		   echo "Feil ved henting av bok";
           $e->getMessage();
           return null;
		}

    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        if(isset($book->title) && isset($book->author)){
            try {
                $query = $this->db->prepare("INSERT INTO book(title, author,description) VALUES (:title, :author,:des)");
                $query->bindValue(':title', $book->title, PDO::PARAM_STR);
                $query->bindValue(':author', $book->author, PDO::PARAM_STR);
                $query->bindValue(':des', $book->description, PDO::PARAM_STR);

                if(!empty($_POST['title']) && !empty($_POST['author'])){
                $query->execute();
                $book->id = $this->db->lastInsertId();
                }else {
                echo "Title and Author can't be empty!";
                $view = new ErrorView();
				$view->create();
                }
            } catch (Exception $e) {
                $view = new ErrorView();
				$view->create();
                $e->getMessage();
            }
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {

        $id = $book->id;
        $book->title = $_POST['title'];
        $book->author = $_POST['author'];
        $book->description = $_POST['description'];
        if(!empty($book->title) && !empty($book->author)){
            try {
                $query = $this->db->prepare("UPDATE book SET title = :title, author = :author, description = :des WHERE id =$id");
                $query->bindValue(':title', $book->title, PDO::PARAM_STR);
                $query->bindValue(':author', $book->author, PDO::PARAM_STR);
                $query->bindValue(':des', $book->description, PDO::PARAM_STR);
                $query->execute();
            } catch (Exception $e) {
                echo "error..!";
                $e->getMessage();
                $view = new ErrorView();
                $view->create();
            }
        } else echo "Title and author must contain something!";
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        if($id){
            try {
                $query = $this->db->prepare("DELETE FROM book WHERE id = :id");
                $query->bindValue(':id',$id, PDO::PARAM_STR);
                $query->execute();
            } catch (Exception $e) {
                $e->getMessage();
                $view = new ErrorView();
                $view->create();
            }

        }
    }

}

?>
